import socket
import xbox
import time


joy = xbox.Joystick()

s = socket.socket()
s.connect(("192.168.0.114", 9999))

def fmtFloat(n):
    return '{:6.3f}'.format(n)

def fmtInt(n):
    return '{:03d}'.format(n)

print "Xbox controller sample: Press Back button to exit"

lectura = lecturaPrev = lecturaLeft = lecturaPrevLeft = 0
lecturaLeftY = lecturaPrevLeftY = lecturaRightY = lecturaPrevRightY = 0

# Loop until back button is pressed
while not joy.Back():
    time.sleep(0.1)
    # Show connection status
    if joy.connected():
        print "Connected   ",
    else:
        print "Disconnected",
    # Left analog stick
    print "Lx,Ly ",fmtFloat(joy.leftX()),fmtFloat(joy.leftY()),
    # Right trigger
    print "Rtrg ",fmtFloat(joy.rightTrigger()),

    #-----------------Trigger derecho-------------

    t = joy.rightTrigger()
    lectura = int(t *100)*10
    if lectura == 1000:
        lectura = 999

    if t <= 0.999 and lectura != lecturaPrev:
        lecturaPrev = lectura
        s.send('h')
        s.send(fmtInt(lectura))

    #-----------------Trigger izquierdo------------

    j = joy.leftTrigger()
    lecturaLeft = int(j *100) * 10
    
    if lecturaLeft == 1000:
        lecturaLeft = 999

    if j <= 1.000 and lecturaLeft != lecturaPrevLeft:
        lecturaPrevLeft = lecturaLeft
        s.send('a')
        s.send(fmtInt(lecturaLeft))

    #-----------------Joystick izquierdo-----------

    jLeft = joy.leftY()
    lecturaLeftY = int(jLeft *100) * 10

    if lecturaLeftY == 1000:
        lecturaLeftY = 999

    if jLeft <= 1.000 and lecturaLeftY != lecturaPrevLeftY:
        lecturaPrevLeftY = lecturaLeftY
        s.send('o')
        s.send(fmtInt(lecturaLeftY))
        
    #-----------------Joystick derecho-----------

    jRight = joy.rightY()
    lecturaRightY = int(jRight *100) * 10

    if lecturaRightY == 1000:
        lecturaRightY = 999

    if jRight <= 1.000 and lecturaRightY != lecturaPrevRightY:
        lecturaPrevRightY = lecturaRightY
        s.send('p')
        s.send(fmtInt(lecturaRightY))

    # A/B/X/Y buttons
    print "Buttons ",
    if joy.A():
        mensaje = 'A'
        s.send('A')
        print "A",
    else:
        print " ",
    if joy.B():
        mensaje = 'B'
    	s.send(mensaje)
        print "B",
    else:
        print " ",
    if joy.X():
    	mensaje = 'X'
    	s.send(mensaje)
        print "X",
    else:
        print " ",
    if joy.Y():
    	mensaje = 'Y'
    	s.send(mensaje)
        print "Y",
    else:
        print " ",
    # Dpad U/D/L/R
    print "Dpad ",
    if joy.dpadUp():
    	mensaje = 'U'
    	s.send(mensaje)
        print "U",
    else:
        print " ",
    if joy.dpadDown():
    	mensaje = 'D'
    	s.send(mensaje)
        print "D",
    else:
        print " ",
    if joy.dpadLeft():
    	mensaje = 'L'
    	s.send(mensaje)
        print "L",
    else:
        print " ",
    if joy.dpadRight():
    	s.send(mensaje)
    	mensaje = 'R'
        print "R",
    else:
        print " ",

    #s.send(mensaje)
        
    # Move cursor back to start of line
    print chr(13),
# Close out when done
joy.close()
s.close()