import RPi.GPIO as GPIO
from time import sleep
import socket

# variables del socket 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("", 9999))
s.listen(1)
sc, addr = s.accept()

# variables de gpio

pinMotorDerecho	= 7
pinMotorLeft 	= 8
pinMotorSup		= 10
pinMotorInf		= 12

veloRight 	= veloLeft = veloSup = veloInf = 5

GPIO.setmode(GPIO.BOARD)
GPIO.setup(pinMotorDerecho,GPIO.OUT)
GPIO.setup(pinMotorLeft,GPIO.OUT)
GPIO.setup(pinMotorSup,GPIO.OUT)
GPIO.setup(pinMotorInf,GPIO.OUT)

motorDerecho   = GPIO.PWM(pinMotorDerecho,50)
motorIzquierdo = GPIO.PWM(pinMotorLeft,50)
motorSup       = GPIO.PWM(pinMotorSup,50)
motorInf 	   = GPIO.PWM(pinMotorInf,50)

motorDerecho.start(10)
motorIzquierdo.start(10)
motorSup.start(10)
motorInf.start(10)

cycling = True

print 'Esperando senal para arrancar motores'

try:
	while cycling:
		#motores para adelante y para atras
		motorDerecho.ChangeDutyCycle(veloRight)
		motorIzquierdo.ChangeDutyCycle(veloLeft)

		#motores para arriba y para abajo
		motorSup.ChangeDutyCycle(veloSup)
		motorInf.ChangeDutyCycle(veloInf)
		
		#senal del socket
		res = sc.recv(1)
		print 'res --> ', res
		
		if res == 'h':
			h = sc.recv(3)
			print 'h --> ', h ,'\n'
			h = int(h) - 300
			h = (h * 500) / 700
			veloLeft = (500+h)/100
			print 'PWM motorLeft: ', veloLeft

		if res == 'a':
			a = sc.recv(3)
			print 'a --> ', a ,'\n'
			a = int(a) - 300
			a = (a * 500) / 700
			veloRight = (500+a)/100
			print 'PWM motorRight: ', veloRight

		if res == 'o':
			o = sc.recv(3)
			print 'o --> ', o ,'\n'
			o = int(o) - 300
			o = (o * 500) / 700
			veloSup = (500+o)/100
			print 'PWM motorSup: ', veloSup

		if res == 'p':
			p = sc.recv(3)
			print 'p --> ', p ,'\n'
			p = int(p) - 300
			p = (p * 500) / 700
			veloInf = (500+p)/100
			print 'PWM motorInf: ', veloInf

		if res == '9':
			cycling = False
			
		sc.send(res)
finally:
	motorDerecho.stop()
	motorIzquierdo.stop()
	motorSup.stop()
	motorInf.stop()
	print 'veloRight var setting is: '
	print '',veloRight
	
motorDerecho.stop()
motorIzquierdo.stop()
motorSup.stop()
motorInf.stop()

GPIO.cleanup()

sc.close()
s.close()
